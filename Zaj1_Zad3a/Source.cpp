#include <array>
#include <cmath>
#include <iostream>
#include <GL/glut.h>

struct Point
{
	GLfloat x;
	GLfloat y;
};

const GLfloat PI = 3.1415926535f;
const GLfloat radius = 50.0f;
const int n = 10;

Point center{ 100.0f, 100.0f };
std::array<Point, n> vertices;

GLfloat stepX = 1.0f;
GLfloat stepY = 1.0f;

GLfloat windowWidth;
GLfloat windowHeight;

void RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin(GL_POLYGON);
	for (size_t i = 0; i < n; i++)
	{
		const GLfloat x = center.x + radius * std::sinf(2 * PI * ((GLfloat)i / n));
		const GLfloat y = center.y + radius * std::cosf(2 * PI * ((GLfloat)i / n));
		vertices[i].x = x;
		vertices[i].y = y;
		glVertex2f(x, y);
	}
	glEnd();

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glutSwapBuffers();
}

void TimerFunction(int value)
{
	GLfloat minX = 1e10;
	GLfloat maxX = -1e10;
	GLfloat minY = 1e10;
	GLfloat maxY = -1e10;
	for (const auto& vertice : vertices)
	{
		if (vertice.x < minX)
			minX = vertice.x;
		if (vertice.y < minY)
			minY = vertice.y;
		if (vertice.x > maxX)
			maxX = vertice.x;
		if (vertice.y > maxY)
			maxY = vertice.y;
	}

	if (minX < 0 || maxX > windowWidth)
		stepX = -stepX;

	if (minY < 0 || maxY > windowHeight)
		stepY = -stepY;

	if (center.x > windowWidth)
		center.x = windowWidth - (maxX - center.x) - 1.0f;

	if (center.y > windowHeight)
		center.y = windowHeight - (maxY - center.y) - 1.0f;

	center.x += stepX;
	center.y += stepY;

	glutPostRedisplay();
	glutTimerFunc(33, TimerFunction, 1);
}

void SetupRC()
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
}

void ChangeSize(GLsizei w, GLsizei h)
{
	if (h == 0)
		h = 1;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (w <= h)
	{
		windowHeight = 250.0f * h / w;
		windowWidth = 250.0f;
	}
	else
	{
		windowWidth = 250.0f * w / h;
		windowHeight = 250.0f;
	}

	glOrtho(0.0f, windowWidth, 0.0f, windowHeight, 1.0f, -1.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}

void main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Program");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);
	glutTimerFunc(33, TimerFunction, 1);
	SetupRC();
	glutMainLoop();
}