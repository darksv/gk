#include <GL/glut.h>
#include <array>
#include "VectorMath.h"

const int N = 10;
const GLfloat GL_PI = 3.1415f;

static GLfloat xRot = 0.0f;
static GLfloat yRot = 0.0f;

void ChangeSize(int w, int h)
{
	GLfloat fAspect;

	if (h == 0)
		h = 1;
	glViewport(0, 0, w, h);
	fAspect = (GLfloat)w / (GLfloat)h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(35.0f, fAspect, 1.0, 40.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void SetupRC()
{
	GLuint texture;
	int x, y;
	uint8_t pixels[256 * 256];
	GLint iWidth = 256;
	GLint iHeight = 256;

	GLfloat whiteLight[] = { 0.05f, 0.05f, 0.05f, 1.0f };
	GLfloat sourceLight[] = { 0.25f, 0.25f, 0.25f, 1.0f };
	GLfloat lightPos[] = { -10.f, 5.0f, 5.0f, 1.0f };
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	glEnable(GL_LIGHTING);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, whiteLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, sourceLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, sourceLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glEnable(GL_LIGHT0);

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	for (y = 0; y < iHeight; y++)
	{
		for (x = 0; x < iWidth; x++)
		{
			pixels[y * iWidth + x] = rand();
		}
	}

	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, 256, 256, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glEnable(GL_TEXTURE_2D);
}

void SpecialKeys(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
		xRot -= 5.0f;
	if (key == GLUT_KEY_DOWN)
		xRot += 5.0f;
	if (key == GLUT_KEY_LEFT)
		yRot -= 5.0f;
	if (key == GLUT_KEY_RIGHT)
		yRot += 5.0f;
	xRot = (GLfloat)((const int)xRot % 360);
	yRot = (GLfloat)((const int)yRot % 360);

	glutPostRedisplay();
}

void RenderScene()
{
	std::array<GLTVector3, 10> bottomVertices;
	for (int i = 0; i < 10; ++i)
	{
		const GLfloat angle = (GLfloat)i / 10.0f * GL_PI * 2.0;
		const GLfloat x = sin(angle);
		const GLfloat y = cos(angle);

		bottomVertices[i][0] = x;
		bottomVertices[i][1] = 0.0f;
		bottomVertices[i][2] = y;
	}

	GLTVector3 topVertice{ 0.0f, 1.0f, 0.0f };
	GLTVector3 baseCenter{ 0.0f, 0.0f, 0.0f };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();

	glTranslatef(0.0f, -0.25f, -4.0f);
	glRotatef(xRot, 1.0f, 0.0f, 0.0f);
	glRotatef(yRot, 0.0f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_TRIANGLES);

	glNormal3f(0.0f, -1.0f, 0.0f);
	for (int i = 0; i < N; ++i)
	{
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(baseCenter);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(bottomVertices[(i + 1) % N]);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(bottomVertices[i]);
	}

	for (int i = 0; i < N; ++i)
	{
		GLTVector3 normal;
		gltGetNormalVector(bottomVertices[i], bottomVertices[(i + 1) % N], topVertice, normal);
		glNormal3fv(normal);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(bottomVertices[i]);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(bottomVertices[(i + 1) % N]);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(topVertice);
	}

	glEnd();
	glPopMatrix();
	glutSwapBuffers();
}


int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Program 3");
	glutReshapeFunc(ChangeSize);
	glutSpecialFunc(SpecialKeys);
	glutDisplayFunc(RenderScene);
	SetupRC();
	glutMainLoop();
	return 0;
}