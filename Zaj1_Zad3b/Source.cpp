#include <array>
#include <cmath>
#include <iostream>
#include <GL/glut.h>

struct Point
{
	GLfloat x;
	GLfloat y;
};

const GLfloat PI = 3.1415926535f;
const GLfloat radius = 50.0f;
const int n = 10;

Point center{ radius, radius };

GLfloat step = 1.0f;

GLfloat windowWidth;
GLfloat windowHeight;

enum class Direction
{
	Right, Left, Up, Down
};

Direction dir = Direction::Up;

void RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin(GL_POLYGON);
	for (size_t i = 0; i < n; i++)
	{
		const GLfloat x = center.x + radius * std::sinf(2 * PI * ((GLfloat)i / n));
		const GLfloat y = center.y + radius * std::cosf(2 * PI * ((GLfloat)i / n));
		glVertex2f(x, y);
	}
	glEnd();

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glutSwapBuffers();
}

void TimerFunction(int value)
{
	switch (dir)
	{
	case Direction::Right:
		center.x += step;
		break;
	case Direction::Left:
		center.x -= step;
		break;
	case Direction::Up:
		center.y += step;
		break;
	case Direction::Down:
		center.y -= step;
		break;
	}


	if (center.x < radius)
	{
		center.x = radius + 1;
		dir = Direction::Up;
	}
	else if (center.y < radius)
	{
		center.y = radius + 1;
		dir = Direction::Left;
	}
	else if (center.x > windowWidth - radius)
	{
		center.x = windowWidth - radius - 1;
		dir = Direction::Down;
	}
	else if (center.y > windowHeight - radius)
	{
		center.y = windowHeight - radius - 1;
		dir = Direction::Right;
	}

	glutPostRedisplay();
	glutTimerFunc(33, TimerFunction, 1);
}

void SetupRC()
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
}

void ChangeSize(GLsizei w, GLsizei h)
{
	if (h == 0)
		h = 1;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (w <= h)
	{
		windowHeight = 250.0f * h / w;
		windowWidth = 250.0f;
	}
	else
	{
		windowWidth = 250.0f * w / h;
		windowHeight = 250.0f;
	}

	glOrtho(0.0f, windowWidth, 0.0f, windowHeight, 1.0f, -1.0f);
}

void main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Program");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);
	glutTimerFunc(33, TimerFunction, 1);
	SetupRC();
	glutMainLoop();
}